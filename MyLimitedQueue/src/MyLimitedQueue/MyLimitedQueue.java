/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyLimitedQueue;

import java.util.Iterator;

public class MyLimitedQueue extends AbstractQueue {
    
    private int limit = 0;
    private int size = 0;
    private Node first;
    private Node last;
    
    public MyLimitedQueue() {
        this.limit = 10;
    }
    
    public MyLimitedQueue(int size) {
        this.limit = size;
    }

    @Override
    int size() {
        return size;
    }

    @Override
    boolean isEmpty() {
        return size == 0;
    }

    @Override
    boolean isNotEmpty() {
        return size != 0;
    }

    @Override
    Iterator<Node> iterator() {
        return new Iterator<Node>() {
            
            Node current = first;
            
            @Override
            public boolean hasNext() {
                return current != null;
            }

            @Override
            public Node next() {
                Node tmp = current;
                current = current.getNext();
                return tmp;
            }
        };
    }

    @Override
    Node[] toArray() {
        Node[] copy = new Node[size];
        int index = 0;
        Iterator<Node> iterator = iterator();
        while(iterator.hasNext()) {
            copy[index] = iterator.next();
            index++;
        }
        return copy;
    }

    @Override
    void add(Node node) throws IllegalStateException {
        if(size < limit) {
            size++;
            if(last == null) {
                this.last = node;
                this.first = last;
            } else {
                last.setNext(node);
                node.setPrevious(last);
            }
        } else {
            throw new IllegalStateException();
        }    
    }

    @Override
    boolean offer(Node node) {
        if(size < limit) {
            last.setNext(node);
            node.setPrevious(last);
            this.last = node;
            return true;
        }
         return false;
    }

    @Override
    Node poll() {
        Node tmp = first.getNext();
        this.first = tmp;
        first.setPrevious(null);
        return first;
    }

    @Override
    Node peek() {
        return this.first;
    }
        
}
