/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyLimitedQueue;

import java.util.Iterator;  

public abstract class AbstractQueue {
    abstract int size();
    abstract boolean isEmpty();
    abstract boolean isNotEmpty();
    abstract Iterator<Node> iterator();
    abstract Node[] toArray();
    abstract void add(Node node) throws Exception;
    abstract boolean offer(Node node);
    abstract Node poll();
    abstract Node peek();
}
