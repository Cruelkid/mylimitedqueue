/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyLimitedQueue;

public class Node {
    
    private Object value;
    private Node next;
    private Node previous;
    
    public Node(Object value) {
        this.value = value;
    }
    
    public Node() {
        this.value = null;
    }
    
    public Object getValue() {
        return value;
    }
    
    public Node getNext() {
        return next;
    }
    
    public Node getPrevious() {
        return previous;
    }
    
    public void setValue(Object value) {
        this.value = value;
    }
    
    public void setNext(Node next) {
        this.next = next;
    }
    
    public void setPrevious(Node previous) {
        this.previous = previous;
    }
}
